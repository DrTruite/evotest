'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

from numpy import array, dot, random, exp

class NeuronLayer():
    def __init__(self, numberOfNeurons, numberOfInputs, weights):
        self.synapticWeights = weights

class Brain():
    def __init__(self, layer1, layer2, layer3):
        self.layer1 = layer1
        self.layer2 = layer2
        self.layer3 = layer3

    def sigmoid(self, x):
        return 1/(1 + exp(-x))

    def think(self, inputs):
        valuesLayer1 = self.sigmoid(dot(inputs, self.layer1.synapticWeights))
        valuesLayer2 = self.sigmoid(dot(valuesLayer1, self.layer2.synapticWeights))
        valuesLayer3 = self.sigmoid(dot(valuesLayer2, self.layer3.synapticWeights))
        #valuesLayer3 = self.sigmoid(dot(valuesLayer1, self.layer3.synapticWeights))
        return valuesLayer3
