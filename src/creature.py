'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

import pygame
from math import cos, sin, log, ceil
from pygame.sprite import Sprite
from brain import NeuronLayer, Brain

class Creature(Sprite):
    def __init__(self, pos, energy, direction, genetic):
        super().__init__()

        # GENETICS:
        # 0 -> neuronLayer 1
        # 1 -> neuronLayer 2
        # 2 -> neuronLayer 3
        # 3 -> colour
        # 4 -> generation

        self.genetic = genetic

        self.brainLayer1 = NeuronLayer(10, 7, genetic[0])
        self.brainLayer2 = NeuronLayer(10, 10, genetic[1])
        self.brainLayer3 = NeuronLayer(7, 10, genetic[2])
        self.brain = Brain(self.brainLayer1, self.brainLayer2, self.brainLayer3)

        self.age = 0
        self.pos = pos
        self.energy = energy
        self.dead = False
        try:
            self.size = ceil(log(self.energy + 1, 2))
        except:
            self.size = 1
            print("Wrong energy amount on birth: ", self.energy)
        self.colour = genetic[3]
        self.generation = genetic[4]
        self.drawingScale = 10
        self.eyesDistance = 5
        self.direction = direction
        self.walk = False
        self.swim = False
        self.rotate = 0
        self.eat = False
        self.birth = False
        self.birthCounter = 0
        self.eyesInput = [0, 0, 0, 0] # mouth, left, middle, right
        self.memory = [0, 0]

        try:
            self.img = pygame.Surface([self.size*self.drawingScale*2, self.size*self.drawingScale*2])
        except:
            print(self.size*self.drawingScale*2, self.size*self.drawingScale*2)

    def drawSelf(self):
        self.img = pygame.Surface([self.size*self.drawingScale*2, self.size*self.drawingScale*2])
        self.img.set_colorkey(pygame.Color(0, 0, 0))
        pygame.draw.circle(self.img, self.colour, [int(self.size*self.drawingScale), int(self.size*self.drawingScale)], self.size)
        pygame.draw.circle(self.img, pygame.Color(10, 10, 10), [int(self.size*self.drawingScale), int(self.size*self.drawingScale)], self.size + 1, 1)
        #Direction line
        pygame.draw.line(self.img,
                        pygame.Color(10, 10, 10),
                        [int(self.size*self.drawingScale), int(self.size*self.drawingScale)],
                        [self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction)), self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction))],
                        1)
        #Eyes graphics
        pygame.draw.line(self.img,
                        pygame.Color(10, 10, 10),
                        [int(self.size*self.drawingScale), int(self.size*self.drawingScale)],
                        [self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction-0.5)), self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction-0.5))],
                        1)
        pygame.draw.line(self.img,
                        pygame.Color(10, 10, 10),
                        [int(self.size*self.drawingScale), int(self.size*self.drawingScale)],
                        [self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction+0.5)), self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction+0.5))],
                        1)
        if self.eyesInput[0] > 0:
            pygame.draw.circle(self.img, pygame.Color(10, int(2.50 * self.eyesInput[0]), 10), [int(self.size*self.drawingScale + (self.size * cos(self.direction))), int(self.size*self.drawingScale + (-1 * self.size * sin(self.direction)))], 3, 1)
        elif self.eyesInput[0] < 0:
            pygame.draw.circle(self.img, pygame.Color(10, 10, 255), [int(self.size*self.drawingScale + (self.size * cos(self.direction))), int(self.size*self.drawingScale + (-1 * self.size * sin(self.direction)))], 3, 1)
        if self.eyesInput[1] > 0:
            pygame.draw.circle(self.img, pygame.Color(10, int(2.50 * self.eyesInput[1]), 10), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction+0.5))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction+0.5)))], 3, 1)
        elif self.eyesInput[1] < 0:
            pygame.draw.circle(self.img, pygame.Color(10, 10, 255), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction+0.5))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction+0.5)))], 3, 1)
        if self.eyesInput[2] > 0:
            pygame.draw.circle(self.img, pygame.Color(10, int(2.50 * self.eyesInput[2]), 10), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction)))], 3, 1)
        elif self.eyesInput[2] < 0:
            pygame.draw.circle(self.img, pygame.Color(10, 10, 255), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction)))], 3, 1)
        if self.eyesInput[3] > 0:
            pygame.draw.circle(self.img, pygame.Color(10, int(2.50 * self.eyesInput[3]), 10), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction-0.5))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction-0.5)))], 3, 1)
        elif self.eyesInput[3] < 0:
            pygame.draw.circle(self.img, pygame.Color(10, 10, 255), [int(self.size*self.drawingScale + (self.eyesDistance * self.size * cos(self.direction-0.5))), int(self.size*self.drawingScale + (-self.eyesDistance * self.size * sin(self.direction-0.5)))], 3, 1)

    def think(self):
        actionList = self.brain.think([self.energy,
                          self.eyesInput[0], self.eyesInput[1], self.eyesInput[2], self.eyesInput[3],
                          self.memory[0], self.memory[1]])
        self.walk = bool(round(actionList[0]))
        self.rotate = round(actionList[2] - actionList[1])*2
        self.eat = round(actionList[3])
        self.birth = round(actionList[4])
        self.memory = [actionList[5], actionList[6]]

    def update(self):
        if self.walk == False and self.eat == False and self.rotate == 0:
            self.energy -= 10
        else:
            self.energy -= 0.5
        if self.walk == True:
            self.walking()
        if self.rotate != 0:
            self.rotating()
        if self.energy < 0:
            self.energy = 0.1
            self.dead = True
        self.size = ceil(log(self.energy + 1, 2))
        self.drawSelf()

    def getWorldInputs(self, world, tileArray):
        eyesIns = [0, 0, 0, 0]

        #Check tiles
        try:
            if world.terrainArray[int((self.pos[1] + (-1 * self.size * sin(self.direction)))/16)][int((self.pos[0] +  (self.size * cos(self.direction)))/16)] == "water":
                eyesIns[0] = -255
        except IndexError:
            eyesIns[0] = -255
        try:
            if world.terrainArray[int((self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction+0.5)))/16)][int((self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction+0.5)))/16)] == "water":
                eyesIns[1] = -255
        except IndexError:
            eyesIns[0] = -255
        try:
            if world.terrainArray[int((self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction)))/16)][int((self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction)))/16)] == "water":
                eyesIns[2] = -255
        except IndexError:
            eyesIns[0] = -255
        try:
            if world.terrainArray[int((self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction-0.5)))/16)][int((self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction-0.5)))/16)] == "water":
                eyesIns[3] = -255
        except IndexError:
            eyesIns[0] = -255

        #Check vegetation
        for veg in tileArray:
            # check mouth eye
            if veg.pos[0] * 16 < self.pos[0] +  (self.size * cos(self.direction)) < (veg.pos[0] * 16) + 16 and veg.pos[1] * 16 < self.pos[1] + (-1 * self.size * sin(self.direction)) < (veg.pos[1] * 16) + 16:
                eyesIns[0] = veg.growth
            # check left eye
            if veg.pos[0] * 16 < self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction+0.5)) < (veg.pos[0] * 16) + 16 and veg.pos[1] * 16 < self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction+0.5)) < (veg.pos[1] * 16) + 16:
                eyesIns[1] = veg.growth
            # check middle eye
            if veg.pos[0] * 16 < self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction)) < (veg.pos[0] * 16) + 16 and veg.pos[1] * 16 < self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction)) < (veg.pos[1] * 16) + 16:
                eyesIns[2] = veg.growth
            # check right eye
            if veg.pos[0] * 16 < self.pos[0] +  (self.eyesDistance * self.size * cos(self.direction-0.5)) < (veg.pos[0] * 16) + 16 and veg.pos[1] * 16 < self.pos[1] + (-1 * self.eyesDistance * self.size * sin(self.direction-0.5)) < (veg.pos[1] * 16) + 16:
                eyesIns[3] = veg.growth

        self.eyesInput = eyesIns

    def walking(self):
        if self.swim == False:
            self.pos[0] += 10 * cos(self.direction)
            self.pos[1] -= 10 * sin(self.direction)
            self.energy -= 0.1
        else:
            self.pos[0] += 5 * cos(self.direction)
            self.pos[1] -= 5 * sin(self.direction)
            self.energy -= 1
            self.swim = False

    def rotating(self):
        self.direction += 0.2 * self.rotate
        self.energy -= 0.1

    def eating(self, veg):
        self.energy += int(min(20, veg.growth/5))
        veg.getEaten(int(min(20, veg.growth/5)))
        self.energy -= 0.5
