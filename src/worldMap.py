'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

from noise import pnoise2

class worldMap():
    def __init__(self, size, seed):
        self.size = self.width, self.height = size
        self.seed = seed
        self.terrainArray = []
        self.elevation = []
        self.vegetation = []
        self.vegetationArray = []
        self.generateHeightmap(5, 1)
        self.generateVegetationMap(5, 1)
        self.generateMap()

    def generateMap(self):
        for j in range(0, self.height):
            row = []
            vegeRow = []
            for i in range(0, self.width):
                #if self.elevation[j][i] < -0.4:
                    #row.append("10")
                #elif self.elevation[j][i] < -0.3:
                    #row.append("09")
                #elif self.elevation[j][i] < -0.2:
                    #row.append("08")
                #elif self.elevation[j][i] < -0.1:
                    #row.append("07")
                if self.elevation[j][i] < 0:
                    row.append("water")
                elif self.elevation[j][i] < 0.1:
                    row.append("05")
                elif self.elevation[j][i] < 0.2:
                    row.append("04")
                elif self.elevation[j][i] < 0.3:
                    row.append("03")
                elif self.elevation[j][i] < 0.4:
                    row.append("02")
                elif self.elevation[j][i] < 0.5:
                    row.append("01")

                if self.elevation[j][i] > 0:
                    if int((self.vegetation[j][i] + 0.5) * 1000) % 3 == 0: #31
                    #if -0.5 < self.vegetation[j][i] < -0.4:
                        vegeRow.append("carrot")
                    elif int((self.vegetation[j][i] + 0.5) * 1000) % 7 == 0: #37
                    #elif -0.2 < self.vegetation[j][i] < -0.1:
                        vegeRow.append("potatoe")
                    elif int((self.vegetation[j][i] + 0.5) * 1000) % 17 == 0: #41
                    #elif 0.1 < self.vegetation[j][i] < 0.2:
                        vegeRow.append("wheat")
                    elif int((self.vegetation[j][i] + 0.5) * 1000) % 43 == 0:
                    #elif 0.4 < self.vegetation[j][i] < 5:
                        vegeRow.append("mushroom")
                    else:
                        vegeRow.append("x")
                else:
                    vegeRow.append("x")
            self.terrainArray.append(row)
            self.vegetationArray.append(vegeRow)

    def generateHeightmap(self, frequency, redist):
        # Make an empty, flat map
        for y in range(0, self.height):
            row = []
            for x in range(0, self.width):
                row.append(0)
            self.elevation.append(row)
        # Noisify it
        for y in range(0, self.height):
            for x in range(0, self.width):
                nx, ny = x/self.width, y/self.height
                xd, yd = (abs((self.width/2) - x))/(self.width/2), (abs((self.height/2) - y))/(self.height/2)
                d = (xd+yd)/2
                self.elevation[y][x] = (0.5 + (pow(pnoise2(frequency * nx, frequency * ny, 4, base=self.seed), redist)) - d) / 2

    def generateVegetationMap(self, frequency, redist):
        # Make an empty, flat map
        for y in range(0, self.height):
            row = []
            for x in range(0, self.width):
                row.append(0)
            self.vegetation.append(row)
        # Noisify it
        for y in range(0, self.height):
            for x in range(0, self.width):
                nx, ny = x/self.width, y/self.height
                xd, yd = (abs((self.width/2) - x))/(self.width/2), (abs((self.height/2) - y))/(self.height/2)
                d = (xd+yd)/2
                self.vegetation[y][x] = (0.5 + (pow(pnoise2(frequency * nx, frequency * ny, 4, base=self.seed * 2), redist)) - d) / 2
