#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

import sys, pygame, time
from math import cos, sin
from floorTile import floorTile
from plants import Carrot, Potatoe, Wheat, Mushroom
from worldMap import worldMap
from creature import Creature
from numpy import random
from copy import deepcopy

pygame.init()

TILE_SIZE = 16
SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = 100*TILE_SIZE, 50*TILE_SIZE
EDGE_MARGIN = 20
MUTATION_AMOUNT = 1
BLACK = 0, 0, 0

screen = pygame.display.set_mode(SCREEN_SIZE)
playerCamera = pygame.Surface([SCREEN_WIDTH, SCREEN_HEIGHT])
cameraXPos, cameraYPos = 0, 0
cameraScale = 1

popCounter = pygame.font.Font("./resources/Peepo.ttf", 30)

# Create world
world = worldMap([100, 50], int(random.random(1)*1000))
terrainArray = []
for j in range(0, len(world.terrainArray)):
    for i in range(0, len(world.terrainArray[j])):
        terrainArray.append(floorTile([i, j], world.terrainArray[j][i]))

# Create vegetation
tileArray = []
for j in range(0, len(world.vegetationArray)):
    for i in range(0, len(world.vegetationArray[j])):
        if world.vegetationArray[j][i] == "carrot":
            tileArray.append(Carrot([i, j]))
        elif world.vegetationArray[j][i] == "potatoe":
            tileArray.append(Potatoe([i, j]))
        elif world.vegetationArray[j][i] == "wheat":
            tileArray.append(Wheat([i, j]))
        elif world.vegetationArray[j][i] == "mushroom":
            tileArray.append(Mushroom([i, j]))

# Random creature generation
def randomGenetic():
    layer1 = 2 * random.random((7, 10)) - 1
    layer2 = 2 * random.random((10, 10)) - 1
    layer3 = 2 * random.random((10, 7)) - 1
    colour = pygame.Color(int(random.random()*255), int(random.random()*255), int(random.random()*255))
    generation = 1
    return [layer1, layer2, layer3, colour, generation]

# Creature mutation
def mutate(geneticInput):
    newGenetic = deepcopy(geneticInput)
    for weight in newGenetic[0]:
        weight += ((random.random() * 2) - 1) * MUTATION_AMOUNT
    for weight in newGenetic[1]:
        weight += ((random.random() * 2) - 1) * MUTATION_AMOUNT
    for weight in newGenetic[2]:
        weight += ((random.random() * 2) - 1) * MUTATION_AMOUNT
    for i in range(0, 2):
        newGenetic[3][i] += min(max(int(abs((random.random() * 10) - 5)), 255), 0)
    newGenetic[4] += 1
    return newGenetic

creatureArray = []

tm = time.perf_counter()
while True:
    # Add new creature if population count fall below 20
    while len(creatureArray) < 20:
        creatureArray.append(Creature([random.random()*1600, random.random()*800], 100, (random.random()*360 - 180), randomGenetic()))

    # Get mouse events
    mousePos = pygame.mouse.get_pos()

    # Apply events
    if mousePos[1] < EDGE_MARGIN:
        cameraYPos += ((EDGE_MARGIN - mousePos[1]) / 1)
    elif mousePos[1] > (SCREEN_HEIGHT - EDGE_MARGIN):
        cameraYPos -= (EDGE_MARGIN - (SCREEN_HEIGHT - mousePos[1])) / 1
    if mousePos[0] < EDGE_MARGIN:
        cameraXPos += ((EDGE_MARGIN - mousePos[0]) / 1)
    elif mousePos[0] > (SCREEN_WIDTH - EDGE_MARGIN):
        cameraXPos -= (EDGE_MARGIN - (SCREEN_WIDTH - mousePos[0])) / 1

    # Pygame events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
                sys.exit()
            if event.key == pygame.K_r:
                cameraXPos = 0
                cameraYPos = 0
                cameraScale = 1
            if event.key == pygame.K_UP:
                cameraYPos += 5
            if event.key == pygame.K_DOWN:
                cameraYPos -= 5
            if event.key == pygame.K_RIGHT:
                cameraXPos -= 5
            if event.key == pygame.K_LEFT:
                cameraXPos += 5
            if event.key == pygame.K_i:
                cameraScale *= 2
                cameraXPos = cameraXPos - ((SCREEN_WIDTH/2))
                cameraYPos = cameraYPos - ((SCREEN_HEIGHT/2))
            if event.key == pygame.K_k:
                cameraXPos = cameraXPos + ((SCREEN_WIDTH/(2*cameraScale)))
                cameraYPos = cameraYPos + ((SCREEN_HEIGHT/(2*cameraScale)))
                cameraScale /= 2

    # Creature tick 8 times per second
    if (time.perf_counter() - tm) > 0.125:
        tm = time.perf_counter()
        for tile in tileArray:
            tile.tick()
        for creature in creatureArray:
            try:
                if creature.walk == True and world.terrainArray[int(creature.pos[1] / 16)][int(creature.pos[0] / 16)] == "water":
                    creature.swim = True
            except IndexError:
                creature.swim = True
            creature.age += 1
            if creature.age >= 2400:
                creature.dead = True
            if creature.dead == True:
                creatureArray.remove(creature)
            else:
                if creature.eat == True:
                    hasEaten = False
                    for veg in tileArray:
                        if veg.pos[0] * 16 < creature.pos[0] +  (creature.size * cos(creature.direction)) < (veg.pos[0] * 16) + 16 and veg.pos[1] * 16 < creature.pos[1] + (-1 * creature.size * sin(creature.direction)) < (veg.pos[1] * 16) + 16:
                            creature.eating(veg)
                            hasEaten = True
                    if hasEaten == False:
                        creature.energy -= 4
                if creature.energy > 200:
                    creature.birth = True
                if creature.birth == True and creature.energy > 100:
                    if creature.birthCounter >= 100:
                        creatureArray.append(Creature(creature.pos.copy(), min(50, creature.energy/2), (random.random()*360 - 180), mutate(creature.genetic)))
                        creature.energy -= min(50, creature.energy/2)
                        creature.birthCounter = 0
                    else:
                        creature.birthCounter += 1
                creature.getWorldInputs(world, tileArray)
                creature.think()

    maxGeneration = 1
    for creature in creatureArray:
        if creature.generation > maxGeneration:
            maxGeneration = creature.generation

    # Drawing
    screen.fill(BLACK)
    for i in range(0, int(SCREEN_WIDTH/TILE_SIZE)):
        for j in range(0, int(SCREEN_HEIGHT/TILE_SIZE)):
            screen.blit(terrainArray[0].img, [i * TILE_SIZE , j * TILE_SIZE])
    playerCamera.fill(BLACK)
    for terrain in terrainArray:
        playerCamera.blit(terrain.img, [i * TILE_SIZE for i in terrain.pos])
    for tile in tileArray:
        playerCamera.blit(tile.img, [i * TILE_SIZE for i in tile.pos])
    for creature in creatureArray:
        creature.update()
        playerCamera.blit(creature.img, [creature.pos[0]-creature.size*creature.drawingScale, creature.pos[1]-creature.size*creature.drawingScale])


    screen.blit(pygame.transform.rotozoom(playerCamera, 0, cameraScale), [cameraXPos, cameraYPos])

    # UI Drawing
    screen.blit(popCounter.render("Population: ", False, pygame.Color(0, 0, 0)), [10, 0])
    screen.blit(popCounter.render(str(len(creatureArray)), False, pygame.Color(0, 0, 0)), [160, 0])

    screen.blit(popCounter.render("Max generation: ", False, pygame.Color(0, 0, 0)), [10, 32])
    screen.blit(popCounter.render(str(maxGeneration), False, pygame.Color(0, 0, 0)), [230, 32])

    pygame.display.flip()
