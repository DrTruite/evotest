'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

import pygame

class floorTile():
    def __init__(self, pos, colour):
        self.pos = pos
        self.img = pygame.image.load("./resources/images/elevation_" + colour + ".png")
