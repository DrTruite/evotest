'''
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of evoTest.

evoTest is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

evoTest is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with evoTest.  If not, see <https://www.gnu.org/licenses/>.
'''

import pygame

class Plant():
    def __init__(self, pos):
        self.pos = pos
        self.growth = 100
        self.imgArray = []

    def tick(self):
        self.growth += 0.1
        if self.growth > 100:
            self.growth = 100
        self.img = self.imgArray[int(self.growth * (len(self.imgArray) - 1) / 100)]

    def getEaten(self, amount):
        self.growth -= amount

class Carrot(Plant):
    def __init__(self, pos):
        super().__init__(pos)
        self.imgArray.append(pygame.image.load("./resources/images/carrots_stage_0.png"))
        self.imgArray.append(pygame.image.load("./resources/images/carrots_stage_1.png"))
        self.imgArray.append(pygame.image.load("./resources/images/carrots_stage_2.png"))
        self.imgArray.append(pygame.image.load("./resources/images/carrots_stage_3.png"))
        self.img = self.imgArray[0]

class Potatoe(Plant):
    def __init__(self, pos):
        super().__init__(pos)
        self.imgArray.append(pygame.image.load("./resources/images/potatoes_stage_0.png"))
        self.imgArray.append(pygame.image.load("./resources/images/potatoes_stage_1.png"))
        self.imgArray.append(pygame.image.load("./resources/images/potatoes_stage_2.png"))
        self.imgArray.append(pygame.image.load("./resources/images/potatoes_stage_3.png"))
        self.img = self.imgArray[0]

class Wheat(Plant):
    def __init__(self, pos):
        super().__init__(pos)
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_0.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_1.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_2.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_3.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_4.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_5.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_6.png"))
        self.imgArray.append(pygame.image.load("./resources/images/wheat_stage_7.png"))
        self.img = self.imgArray[0]

class Mushroom(Plant):
    def __init__(self, pos):
        super().__init__(pos)
        self.imgArray.append(pygame.image.load("./resources/images/nether_wart_stage_0.png"))
        self.imgArray.append(pygame.image.load("./resources/images/nether_wart_stage_1.png"))
        self.imgArray.append(pygame.image.load("./resources/images/nether_wart_stage_2.png"))
        self.img = self.imgArray[0]
